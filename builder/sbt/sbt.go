package sbt

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven/v2/builder"
)

const (
	flagSbtOpts = "sbt-opts"

	pathProjectDotGraph = "dependencies-compile.dot"
	pathPluginOutputDir = "target"
)

// Builder generates dependency lists for sbt projects
type Builder struct {
	SbtOpts string
}

// Flags returns the CLI flags that configure the sbt command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagSbtOpts,
			Usage:   "Optional CLI arguments for sbt",
			Value:   "",
			EnvVars: []string{"SBT_CLI_OPTS"},
		},
	}
}

// Configure configures the sbt dependency listing command
func (b *Builder) Configure(c *cli.Context) error {
	b.SbtOpts = c.String(flagSbtOpts)
	return nil
}

// Build generates a dependency list for sbt projects
func (b Builder) Build(input string) (string, error) {
	if err := b.generateDotReport(input); err != nil {
		return "", nil
	}

	src := filepath.Join(filepath.Dir(input), pathPluginOutputDir, pathProjectDotGraph)
	if _, err := os.Stat(src); err != nil {
		return "", fmt.Errorf("dot graph output file is missing from %s", src)
	}

	output := filepath.Join(filepath.Dir(input), pathProjectDotGraph)
	if err := os.Rename(src, output); err != nil {
		return "", err
	}

	return output, nil
}

// generateDotReport generates a dependency dot graph for the current project
func (b Builder) generateDotReport(input string) error {
	args := strings.Fields(b.SbtOpts)
	args = append(args, "dependencyDot")
	cmd := exec.Command("sbt", args...)
	cmd.Dir = filepath.Dir(input)
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

func init() {
	builder.Register("sbt", &Builder{})
}
